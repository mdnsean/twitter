I created this Twitter-like app by following Michael Hartl's tutorial (railstutorial.org). It features signup, login, posts, followers, image upload, and a news feed.

This was my introduction to Ruby on Rails, including:

1) Models (migrations / filters / validations / associations) 
2) Views (and partials)
3) Controllers
4) Test suite
5) Gems / Rake commands / Git / Heroku
6) ... and a little bit of Html / CSS / JS / SQL
